import 'package:flutter/material.dart';
import 'package:flutter_challenge/ui/DetailPage.dart';
import 'package:flutter_challenge/ui/HomePage.dart';

void main() {
  runApp(new MaterialApp(
    theme: ThemeData(
      primaryColor: Colors.blue[700],
      accentColor: Colors.orange,
    ),
    home: new HomePage(),
    routes: {
      DetailPage.routeName: (ctx) => DetailPage()
    },
  ));
}
