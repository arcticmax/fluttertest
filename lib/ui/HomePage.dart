import 'package:flutter/material.dart';

import 'DetailPage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List contacts = [];
  List filteredContacts = [];
  bool isSearching = false;

  getContacts() {
    final contacts = <String>[
      'Antonio Sanchez',
      'Roberto Martinez',
      'Oscar Gonzalez',
      'Alma Arechiga',
      'Rene Ochoa',
      'Jose Rios',
      'Paola Martinez',
      'Alan Martinez',
      'Israel de la Cruz',
      'Catalina Ochoa',
      'Hugo Gonzalez',
      'Alfonso Valverde',
      'Luis Suarez',
      'Rigo Tovar',
      'Teresa Salas'
    ];
    return contacts;
  }

  @override
  void initState() {
    contacts = filteredContacts = getContacts();
    super.initState();
  }

  void _filterContacts(value) {
    setState(() {
      filteredContacts = contacts
          .where((contact) =>
            contact.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: !isSearching
            ? Text('Contacts')
            : TextField(
          onChanged: (value) {
            _filterContacts(value);
          },
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              hintText: "Search Contact Here",
              hintStyle: TextStyle(color: Colors.white)),
        ),
        actions: <Widget>[
          isSearching
              ? IconButton(
            icon: Icon(Icons.cancel),
            onPressed: () {
              setState(() {
                this.isSearching = false;
                filteredContacts = contacts;
              });
            },
          )
              : IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              setState(() {
                this.isSearching = true;
              });
            },
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: filteredContacts.length > 0
            ? ListView.builder(
            itemCount: filteredContacts.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed(DetailPage.routeName,
                      arguments: filteredContacts[index]);
                },
                child: Card(
                  elevation: 10,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 8),
                    child: Text(
                      filteredContacts[index],
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ),
              );
            })
            : Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

}